import collections
from warsexceptions import *
import copy

def mergeDicts(d1, d2, noCopy=True):
	for k,v in d2.items():
		if isinstance(v, collections.Mapping):
			if k in d1:
				mergeDicts( d1[k], v)
			else:
				if not noCopy:
					d1[k] = copy.copy(v)
				else:
					d1[k] = v
		else:
			if not noCopy:
				d1[k] = copy.copy(v)
			else:
				d1[k] = v

class DataObject(object):

	def __init__(self, data={}):
		super(DataObject, self).__init__()
		self.data = data

	def __getitem__(self, key):
		return self.data[key]

	def setData(self, d):	
		self.data = d

	def getData(self):
		return self.data

	def updateData(self, d, copy=True):
		mergeDicts(self.data, d, noCopy= not copy)

	def makeBackup(self):
		self.backup = copy.deepcopy(self.data)

	def rollback(self):
		self.data = self.backup
		self.backup = None
	
	def subtractValue(self, valueName, s):
		if valueName not in self.data:
			raise DataResourceError(valueName, responseinfo="You don't have enough %s" % valueName)
		if self.data[valueName] - s >= 0:
			self.data[valueName] -= s
		else:
			raise DataResourceError(valueName, responseinfo="You don't have enough %s" % valueName)

	def increaseValue(self, valueName, s, top=False):
		if valueName not in self.data:
			raise DataResourceError(valueName)	
		self.data[valueName] += s
		if top:
			self.data[valueName] = min(self.data[valueName],
					self.specs["data"][valueName])

	
