import json
from dataObject import *
from warsexceptions import *
import math
import traceback
import glob
from twisted.python import log

from movementComponent import componentMovement
from unitComponent import componentUnit
from weaponComponent import componentWeapon
from healthComponent import componentHealth
from captureComponent import componentCapture
from producerComponent import componentProducer
from builderComponent import componentBuilder
from resupplyComponent import componentResupply
from tileComponent import componentTile

def get_entity(req):
	resp = transaction.respMessage(req, True)
	resp["entity"] = getEntity( req["type"] )
	return resp

def getEntity(t):
	if(t not in Entity.types):
		return None
	return Entity.types[t]

class Entity(DataObject):

	types = dict()
	
	def __init__(self, game, data):
		super(Entity, self).__init__()
		self.setData(data)
		self.game = game
		self.map = game.map
		self.specs = getEntity(self.data["type"])
		self.components = dict()
		self.actions = dict()
		self.all_tables = set()
		self.all_tables.add("entities")
		self.changed_tables = set()

		if "components" in Entity.types[self.data["type"]]:
			for k, c in Entity.types[self.data["type"]]["components"].items():
				comp = eval("component" + k)(self, specs=c)
				self.components[k] = comp

	def getData(self):
		return self.data

	def getTeam(self):
		return self.game.getTeams()[ self.data["teamid"]]

	def __str__(self):
		if "entityid" in self.data:
			return "entity (%d)" % self.data["entityid"]
		else:
			return "entity (unknown)"

	def hasComponent(self, c):
		return (c in self.components)

	def onSpawn(self):
		"""
		Called when the entity is spawned into the world.
		Copies start data from the entitytype`s specification
		"""
		#TODO raise instead of magic return value
		for (k,v) in self.specs["data"].items():
			print("adding specs from %s" % k)
			self.data[k] = v

		ret = True
		for comp in self.components.itervalues():
			try:
				if comp.onSpawn() == False:
					ret = False
			except AttributeError:
				print("No onSpawn")
		return ret


	def newRound(self):
		"""
		Called when a new round begins.
		Resets time.
		Calls newRound on all components.
		"""
		self.changed_tables.add("entities")

		for c in self.components.itervalues():
			try:
				c.newRound()
			except AttributeError:
				pass

	def newDay(self):
		"""
		Called when a new day begins (turn has wraped around
		to own team again).
		"""
		for c in self.components.itervalues():
			try:
				c.newDay()
			except AttributeError:
				pass
	
	def getTile(self):
		return self.getTileAt( self.data["xpos"], self.data["ypos"])

	def getTileAt(self, x, y):
		ents = self.getEntitiesAtMyPos()
		for e in ents:
			if not e.isDestroyed() and e.hasComponent("Tile"):
				return e.components["Tile"].getTile()
		return self.map.getTile( x, y)
			
	
	def isDestroyed(self):
		if "health" in self.data:
			return self.data["health"] == 0
		else:
			return False

	def distanceTo(self, x, y):
		return math.ceil(math.sqrt( abs(self.data["xpos"] - x)**2 + abs(self.data["ypos"] - y)**2 ))

	def getEntitiesAtMyPos(self):
		return self.getEntitiesAtPos(self.data["xpos"], self.data["ypos"])

	def getEntitiesAtPos(self,x, y):
		l = list()
		for u in self.game.all_entities.itervalues():
			if(x == u.data["xpos"] and y == u.data["ypos"] and u != self):
				l.append(u)
		return l

	def getPosFromDirection(self, direction):
		x = self.data["xpos"]
		y = self.data["ypos"]

		if(direction == "W"):
			x -= 1
		elif(direction == "E"):
			x += 1
		elif(direction == "N"):
			y -= 1
		elif(direction == "S"):
			y += 1
		else:
			raise IllegalParam(param="direction", info="Illegal direction")

		return (x,y)


	def message(self, command, param):
		"""
		Received a command from the client.
		"""
		if self.isDestroyed():
			raise ParamError(responseinfo="You cannot send commands to dead units :(")

		if command not in self.actions:
			raise ParamError(responseinfo="This entity does not have a component that provides this action.")
		self.makeBackup()
		try:
			return self.actions[command](param)
		except Exception:
			self.rollback()
			raise

def loadEntityType(t):
	if t not in Entity.types:
		print "\t"+t
		f = open("data/entities/" + t + ".json")
		e = json.load(f)
		Entity.types[t] = e
		f.close()
		if "base" in e:
			for sub in e["base"]:
				print "\t\t" + sub
				basec = copy.deepcopy(loadEntityType(sub))
				mergeDicts(basec, e)
				e = basec

		Entity.types[t] = e
	return Entity.types[t]



print "loading entitytypes"
for filename in glob.glob("data/entities/*.json"):
	loadEntityType( filename.split("/")[-1].split(".")[0] )
print "done"

