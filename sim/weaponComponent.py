from component import Component
from warsexceptions import *
import random
from twisted.python import log

class componentWeapon(Component):
    """
    The component responsible for simulating weapons and fire commands
    """
    
    def __init__(self, parent, specs=dict()):
        super(componentWeapon, self).__init__(parent, specs)
        parent.actions["fire"] = self.fireCommand
        parent.all_tables.add("units")

    def calculateDamage(self, weapon, targets):
        damages = [0]*len(targets)
        for i in xrange(weapon["roundsfired"]):
            #Scale damage according to own health
            dam = weapon["damage"] * self.parent.data["health"]/100
            for i, target in zip(xrange(0, len(targets)), targets):
                if "Health" in target.components:
                    #Apply damage to target
                    damages[i] += target.components["Health"].calculateDamage(
                                    weapon["type"], dam, weapon["partialprob"], weapon["hitprob"])
        log.msg("Damage: %s" % ", ".join(str(x) for x in damages))
        return damages

    def fire(self, coordinate):
        """
        Fire upon a target.

        @type target: Entity
        @param target: the entity to be fired upon
        @rtype int
        @return The damage dealt. Errorcode if negative
        """
        weapon = self.specs["weapons"][0]
        x,y = coordinate

        log.msg("Weapon %s" % weapon["name"])
        distance = self.parent.distanceTo(x, y)
        log.msg("Distance: %d" % distance)
        if( distance > weapon["range"]):
            raise ParamError(responseinfo="Target out of range")
        if( distance < weapon["minrange"]):
            raise ParamError(responseinfo="Target too close")
                
        self.parent.subtractValue("time", weapon["time"])

        targets = self.parent.getEntitiesAtPos(x,y)
        log.msg("ATTACK: %s -> %s" % (self.parent["type"], ", ".join( x["type"] for x in targets)))
                
        damages = self.calculateDamage(weapon, targets)
        tot = sum(damages)
        log.msg("Total damage", tot)

        for damage, target in zip(damages, targets):
            if damage > 0:
                target.components["Health"].applyDamage(damage)

        return tot
    

    def fireCommand(self, message):
        """
        Called when received a command to fire.

        @type message: dict
        @param message: the command description
        @rtype dict
        @return a valid response
        """
        tot = self.fire((message["xpos"], message["ypos"]))
                
        #Return fire
        targets = self.parent.getEntitiesAtPos(message["xpos"], message["ypos"])
        for target in targets:
            if not target.isDestroyed():
                if "Weapon" in target.components:
                    try:
                        target.components["Weapon"].fire((self.parent["xpos"], self.parent["ypos"]))
                    except (ParamError, DataResourceError):
                        pass
        return tot
        
        
