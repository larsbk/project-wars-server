from twisted.internet import reactor
import psycopg2

connection = None
cursor = None
serverid = None

def getServerId():
	global serverid
	return serverid

def setServerId(i):
	global serverid
	serverid = i

def createConnection(addr, port, user, pw, name):
	try:
		print "Connecting to db"
		global connection
		global cursor
		connection = psycopg2.extras.RealDictConnection(
			"dbname=%s user=%s password=%s host=%s port=%d" %
			(name, user,pw,addr,port))
		cursor = connection.cursor()
		print "Success!"
		return True
	except psycopg2.OperationalError as e:
		panic(e)
		return False

def panic(e):
	print "PANIC!!! " + str(e)
	connection = None
	cursor = None
	reactor.stop()
	raise SystemExit()

def commit():
	print "sql commit"
	try:
		return connection.commit()
	except psycopg2.OperationalError as e:
		panic(e)

def rollback():
	print "sql rollback"
	try:
		return connection.rollback()
	except psycopg2.OperationalError as e:
		panic(e)

def query(s, param=() ):
	print "sql query: %s" % cursor.mogrify(s, param)
	try:
		return cursor.execute(s, param)
	except psycopg2.OperationalError as e:
		panic(e)

def fetch():
	print "sql fetch"
	try:
		return cursor.fetchone()
	except psycopg2.OperationalError as e:
		panic(e)
def fetchAll():
	print "sql fetchall"
	try:
		return cursor.fetchall()
	except psycopg2.OperationalError as e:
		panic(e)

def other(m, *p):
	print "sql other"
	try:
		return cursor.m(p)
	except psycopg2.OperationalError as e:
		panic(e)

		
