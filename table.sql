DROP TABLE users CASCADE;
DROP TABLE players CASCADE;
DROP TABLE games CASCADE;
DROP TABLE participants CASCADE;
DROP TABLE entities CASCADE;
DROP TABLE servers CASCADE;
DROP TABLE units CASCADE;


CREATE TABLE players (
	userid serial primary key NOT NULL,
	username varchar(32) unique NOT NULL,
	score integer DEFAULT 0 NOT NULL
);

CREATE TABLE users (
	userid integer references players ON DELETE CASCADE,
	password varchar(256) NOT NULL,
	privilege varchar(10) DEFAULT 'u' NOT NULL,
	mail varchar(320) DEFAULT '' NOT NULL,
	primary key(userid)
);

CREATE TABLE servers (
	addr varchar(100) NOT NULL,
	port integer NOT NULL,
	ws varchar(100) NOT NULL,
	timestamp timestamp DEFAULT clock_timestamp() NOT NULL,
	serverid serial primary key
);

CREATE TABLE games (
	gameid serial primary key,
	serverlock integer references servers ON DELETE SET DEFAULT DEFAULT NULL,
	availability varchar(10) DEFAULT 'a' NOT NULL,
	mapid varchar(128) NOT NULL,
	turn integer DEFAULT 1 NOT NULL,
	totalturns integer DEFAULT 0 NOT NULL
);

CREATE TABLE participants (
	gameid integer references games ON DELETE CASCADE NOT NULL,
	userid integer references players ON DELETE SET DEFAULT DEFAULT 0,
	teamid integer DEFAULT 0 NOT NULL,
	co varchar(32) DEFAULT '' NOT NULL,
	resources integer DEFAULT 0 NOT NULL,
	power integer DEFAULT 0 NOT NULL,
	score integer DEFAULT 0 NOT NULL,
	primary key(gameid, teamid),
	check(teamid >= 0)
);

CREATE TABLE entities (
	entityidsecret serial NOT NULL PRIMARY KEY, --TODO: maybe bigserial?
	entityid integer NOT NULL,
	gameid integer references games ON DELETE CASCADE NOT NULL,
	teamid integer DEFAULT -1 NOT NULL,
	type varchar(32) NOT NULL,
	xpos integer NOT NULL,
	ypos integer NOT NULL,
	health integer DEFAULT 100 NOT NULL,
	unique(entityid, gameid)
);

CREATE TABLE units (
	entityidsecret integer references entities ON DELETE CASCADE,
	fuel integer DEFAULT 100 NOT NULL,
	time integer DEFAULT 100 NOT NULL,
	ammo1 integer DEFAULT 0 NOT NULL,
	ammo2 integer DEFAULT 0 NOT NULL,
	primary key(entityidsecret)
);

GRANT ALL ON users TO wars;
GRANT ALL ON players TO wars;
GRANT ALL ON entities TO wars;
GRANT ALL ON units TO wars;
GRANT ALL ON participants TO wars;
GRANT ALL ON games TO wars;
GRANT ALL ON servers TO wars;

GRANT ALL ON games_gameid_seq TO wars;
GRANT ALL ON entities_entityidsecret_seq TO wars;
GRANT ALL ON servers_serverid_seq TO wars;
GRANT ALL ON players_userid_seq TO wars;

INSERT INTO players (userid, username) VALUES (DEFAULT, 'larsbk');
INSERT INTO users (userid, password, privilege, mail) VALUES (1, '123', 'ua', 'lars.bjorlykke.kristiansen@gmail.com');

INSERT INTO players (userid, username) VALUES (DEFAULT, 'aleksiml');
INSERT INTO users (userid, password, privilege) VALUES (2, 'qwerty', 'ua');


