echo "running: python listen.py config.json"
while :
do
	git pull origin master
	git submodule update
	DATE=$(date +"%Y%m%d%H%M")
	echo "Starting"
	python listen.py config.json
	echo "Exited!"
	sleep 2
done
echo "Server exited!"
