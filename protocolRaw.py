from twisted.internet.protocol import Protocol, Factory
import logger
from transaction import Transaction

class RawProtocol(Protocol, logger.logger):


	def __init__(self, factory, addr):
		super(RawProtocol, self).__init__()
		self.factory = factory
		self.addr = addr


	def connectionMade(self):
		"""
		Called by Twisted when a connection has been established
		"""
		if(self.factory.shutdown == True): #Dont accept connections if the server is restarting
			self.log("aborted connection (shutdown)")
			self.transport.loseConnection()
			return
		self.factory.activeConnections += 1
		self.log("connected")
		self.transaction = Transaction(self)

	def __str__(self):
		return "WarsProtocol(%s)" % self.addr
		
	def dataReceived(self, data):
		"""
		Called by Twisted when new bytes has been received.
		"""
		self.log("got data")
		self.transaction.dataReceived(data)

	def kill(self):
		self.transport.loseConnection()
	
	def write(self, msg):
		self.transport.write(msg)
		
	def connectionLost(self, reason):
		"""
		Called by Twisted when a connection has been lost
		"""
		print "close " + str(self) + ": " + str(reason)
		self.transaction.connectionLost()
		if(self.factory != None):
			self.factory.down()

class RawProtocolFactory(Factory) :
	"""
	Factory that creates WarsProtocol instances
	"""

	def __init__(self):
		self.activeConnections = 0
		self.shutdown = False
	
	def down(self):
		"""
		Decrement the connections counter
		"""
		self.activeConnections -= 1
		if(self.shutdown == True and self.activeConnections == 0):
			reactor.stop()
	
	def buildProtocol(self, addr):
		print "New connection from %s" % addr
		return RawProtocol(self, addr)

