from dataObject import *

class Player(DataObject):
	def __init__(self, d):
		super(Player, self).__init__()
		self.setData(d)

	def hasJoined(self):
		return self.data["userid"] != None


