import psycopg2
import re
import json
import user
import game
import traceback
import sqlWrapper as sql
import logger
from warsexceptions import *
import map


class Transaction(logger.logger):
	"""
	The class that performs the communications with clients.
	An instance of this class is created for each connection
	"""
	logaddr = None
	eofRegex = re.compile("\r?\n\r?\n")

	#request name : (auth req string, lambda)
	requestHandlers = {
		"listgames"  : ("",  lambda r, t, g: game.listAllGames(r)),
		"status"     : ("",  lambda r, t, g: t.getStatus(r)),
		"pw_auth"    : ("",  lambda r, t, g: t.user.authenticate_pw(r, t)),
		"zkpp_chall" : ("",  lambda r, t, g: t.user.authenticate_zkpp(r, t)),
		"zkpp_auth"  : ("",  lambda r, t, g: t.user.responseauth_zkpp(r, t)),
		"restart"    : ("a", lambda r, t, g: t.restart(r)),
		"new_user"   : ("a", lambda r, t, g: user.newUser(r)),
		"set_pw"     : ("a", lambda r, t, g: user.setpassword(r)),
		"change_pw"  : ("u", lambda r, t, g: t.user.changePassword(r)),
		"get_user"   : ("",  lambda r, t, g: user.get_user(r)),
		"set_user"   : ("a",  lambda r, t, g: user.set_user(r)),
		"log"        : ("a", lambda r, t, g: t.log("client log: " + r["msg"])),
		"join_game"  : ("u", lambda r, t, g: g.join(t, r)),
		"new_game"   : ("u", lambda r, t, g: game.new_game(t,r)),
		"end_turn"   : ("u", lambda r, t, g: g.endTurn(r,t)),
		"get_game"   : ("",  lambda r, t, g: g.get_state(r)),
		"entity_cmd" : ("u", lambda r, t, g: g.send_command(t, r)), 
		"get_map"    : ("", lambda r, t, g: map.get_map(r)),
		"get_maplist": ("", lambda r, t, g: map.get_maplist(r)),
		"get_tileset": ("", lambda r, t, g: map.get_tileset(r)),
		"entity_type": ("", lambda r, t, g: entity.get_entity(r)), 
	}

	def __init__(self, protocol):
		super(Transaction, self).__init__()
		self.decoder = json.JSONDecoder()
		self.protocol = None
		self.parent = protocol
		self.receive_buffer = ""
		self.user = user.User(self)
		self.messageId = 0
		self.lockedGames = set()

	def addLockGame(self, g):
		self.log("add to unlocklist %s" % str(g))
		self.lockedGames.add(g)
	
	def removeLockGame(self, g):
		self.log("remove from unlocklist %s" % str(g))
		self.lockedGames.remove(g)

	def dataReceived(self, data):
		"""
		Called by Twisted when new bytes has been received.
		"""
		self.log("got data")
		self.receive_buffer += data
		if len(self.receive_buffer.strip()) == 0:
			self.receive_buffer = ""
			return

		while re.search(self.eofRegex, self.receive_buffer) != None:
				try: #Try to interpret this as a json.
					obj,index = self.decoder.raw_decode(self.receive_buffer) #Converts the json to a dict
					self.receive_buffer = self.receive_buffer[index:] #Move the buffer past this json object
					self.receive_buffer = self.receive_buffer.lstrip()
				except ValueError: #Could not be interpretted as json. Might be half a message.
					self.sendJson(protocolViolation(None))
					self.parent.kill()
					self.log("ValueError " + self.receive_buffer)
					return
				try:
					self.interpretJson(obj)
				except Exception: #If some error occurs, we send the error to the client.
					self.log(traceback.format_exc())
					self.sendJson(respMessage(obj, False, detail="crash", info=traceback.format_exc()))

	def interpretJson(self, obj):
		"""
		Interprets the received json
		"""
		self.log("Received from client: \n" + json.dumps(obj, indent=4))

		if("messageid" not in obj):
			self.log("no messageid")
			self.sendJson(protocolViolation(obj))
			return

		if(self.protocol == None):  #Protocol has not been negotiated yet
			self.log("protocol")
			if(obj["type"] == "request" and obj["request"] == "handshake"):
				if(obj["protocol"] == "wars2"):
					self.protocol = obj["protocol"]
					self.sendJson(respMessage(obj, True, info="Connection established successfully"))
					#if("tls" in obj):
					#	if(obj["tls"] == True):
					#		ctx = None
					#		self.transport.startTLS(ctx, self.factory)

				else:
					self.sendJson(respMessage(obj, False, info="Unsupported protocol"))
			else:
				self.sendJson(protocolViolation(obj))
				self.parent.kill()

		else: #Real protocol starts here

			gamei = None
			if("gameid" in obj):
				gamei = game.getGame(obj["gameid"])
				if gamei == None:
					self.sendJson( respMessage(obj, False, detail="gameid") )
					return

			if(obj["type"] == "request"):
				self.log("Got request %s" % obj["request"])
				if(obj["request"] in self.requestHandlers):
					p, f = self.requestHandlers[obj["request"]]
					if(self.user.hasAuth(p) ):
						try:
							resp = f(obj, self, gamei)
							if resp == None:
								resp = respMessage(obj, True, detail="no resp")
						except WarsError as e:
							self.log("Caught WarsError %s" % str(e))
							resp = respMessage(obj, False)
							resp.update( e.getData())
						self.sendJson(resp)
						if gamei != None:
							gamei.pushAll()
					else:
						self.sendJson( respMessage(obj, False, detail="denied", info="Permission denied!"))
				else:
					self.sendJson( respMessage(obj, False, detail="request_not_implemented"))
			else:
				self.sendJson( protocolViolation(obj))

	def getStatus(self, obj):
		return respMessage(obj, True, info=("Users logged in: %d" % self.factory.activeTransactions))

	def connectionLost(self):
		for g in self.lockedGames.copy():
			self.log( "unlocking " + str(g))
			g.save(self)


	def restart(self, obj):
		self.factory.shutdown = True
		self.log("admin restart")
		return respMessage(obj, True, info="The server will restart when all connected clients have disconnected (Including you). No more connections will be accepted")

	def __str__(self):
		if(self.user != None and self.user.authenticated == True):
			return "Connection (%s, %s):" % (self.user.data["username"], str(self.parent))
		else:
			return "Connection (not authorized, "+ str(self.parent) + ")"
		
	def sendJson(self, d):
		"""
		Sends a dict as json to the connected client
		"""
		self.log("sent msg to client:")
		self.log(d)
		d["messageid"] = self.messageId
		self.messageId += 1
		self.parent.write(json.dumps(d, indent=4) + "\r\n\r\n")


def respMessage(msg, success, detail=None, info=None):
	"""
	Utility function. Creates a response to msg.

	@type msg: dict
	@param msg: the message to respond to
	@type success: bool
	@param success: respond with success or failure
	@type detail: string
	@param detail: detail about respone. Why did something fail?
	@type info: string
	@param info: Human readable info about the response. May be printed by the client.
	@rtype dict
	@return The response
	"""
	response = dict()
	response["type"] = "response"
	if(msg != None and "messageid" in msg):
		response["responseid"] = msg["messageid"]
	
	response["responsesuccess"] = success
	if(detail != None):
		response["responsedetail"] = detail
	if(info != None):
		response["responseinfo"] = info
	return response

def protocolViolation(msg):
	"""
	Utility function. Special case of respMessage. Specifies that the message
	sent was a protocol violation.

	@type msg: dict
	@param msg: the message that violated the protocol
	@rtype dict
	@return The response
	"""
	p = respMessage(msg, False, detail="protocol violation", info="Your client is written by 10 drunken monkeys pressing random buttons")
	return p


