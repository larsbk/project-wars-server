from twisted.internet import defer
from txjason.service import JSONRPCError
from txjason import handler
from bson import ObjectId
from rpc import requireArgs
from authPasswordHandler import requireLoggedIn
from gamesHandler import GamesHandler
from twisted.python import log
from saveGame import saveGame, loadGame
from sim.game import Game
from sim.map import getMap, getTileset
from sim.entity import getEntity

@defer.inlineCallbacks
def getGame(gameid, db):
	try:
		defer.returnValue(GamesHandler.games[gameid])
	except KeyError:
		g = Game()
		yield loadGame(db, g, ObjectId(gameid))
		GamesHandler.games[gameid] = g
		defer.returnValue(g)

class GameHandler(handler.Handler):

	def __init__(self, db):
		self.db = db

	@handler.exportRPC(types={"teamid":int, "gameid":basestring}, required=["teamid", "gameid"])
	@requireLoggedIn()
	@defer.inlineCallbacks
	def join(self, user, gameid=None, teamid=None):
		"""
		Join the game with the given gameid as the given team.
		Return the data of the team joined.
		"""
		print (gameid, user, teamid)
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		player = yield game.join(teamid, user)
		user.addJoinedGame(game)
		yield saveGame(self.db, game)
		defer.returnValue(player)

	@defer.inlineCallbacks
	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	def getTeams(self, gameid=None):
		"""
		Return a list of the teams indexed by (teamid - 1) in the given game. 
		"""
		print "getTeams" + str(gameid)
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		r = game.getTeams()
		print "ret: " + str(r)
		yield saveGame(self.db, game)
		defer.returnValue(r)
	
	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	@requireLoggedIn()
	@defer.inlineCallbacks
	def endTurn(self, user, gameid=None):
		"""
		End your turn in the given game.
		Requires that you are logged in as the user controlling the current team.
		"""
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		ret = yield game.endTurn(user)
		game.show()
		yield saveGame(self.db, game)
		defer.returnValue(ret)

	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	@defer.inlineCallbacks
	def getDay(self, gameid=None):
		"""
		Return the current \"day\" in the given game.
		One day passes whenall teams has had their turn.
		"""
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		defer.returnValue( game.getDay())

	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	@defer.inlineCallbacks
	def getTurn(self, gameid=None):
		"""
		Return the teamid of the team who's turn it is.
		"""
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		defer.returnValue(game.getTurn())

	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	@defer.inlineCallbacks
	def getEntities(self, gameid=None):
		"""
		Return the entities in the game in a list of objects.
		List element N is the entities of teamid N+1. Each element in the list
		is a <entityid, entitydata> map.
		"""
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		defer.returnValue( game.getEntities())

	@handler.exportRPC(types={"gameid":basestring}, required=["gameid"])
	@defer.inlineCallbacks
	def getMapid(self, gameid=None):
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		defer.returnValue( game.getMapid() )

	@handler.exportRPC(types={"mapid":basestring}, required=["mapid"])
	def getMap(self, mapid=None):
		m = getMap(mapid)
		return m.getData()

	@handler.exportRPC(types={"tileset":basestring}, required=["tileset"])
	def getTileset(self, tileset=None):
		t = getTileset(tileset)
		return t

	@handler.exportRPC(types={"entity":basestring}, required=["entity"])
	def getEntityType(self, entity=None):
		e = getEntity(entity)
		return e

	@handler.exportRPC(types={"gameid":basestring, "entityid":int, "command":basestring,
		"params":dict},	required=["gameid", "entityid", "command"])
	@requireLoggedIn()
	@defer.inlineCallbacks
	def sendCommand(self, user, gameid=None, entityid=None, command=None, params={}):
		"""
		Send a command to the given entity.
		You need to be logged in as the user who controlls the team of the entity.
		"""
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		ret = yield game.sendCommand(user, entityid, command, params)
		yield saveGame(self.db, game)
		defer.returnValue(ret)

	@handler.exportRPC(types={"gameid":basestring, "turn":int}, required=["gameid", "turn"])
	@defer.inlineCallbacks
	def waitTurn(self, gameid=None, turn=None):
		game = yield defer.maybeDeferred(getGame, gameid, self.db)
		log.msg("Waiting for turn %d" % turn)
		while (not game.teams[turn].isDefeated()) and not game.checkEndGame() and game.getTurn() != turn:
			r = yield game.onTurn
		if game.checkEndGame():
			raise JSONRPCError("game over")
		log.msg("wait turn done! %d" % turn)
		defer.returnValue(game.getTurn())
		
		


