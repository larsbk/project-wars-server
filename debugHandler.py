from twisted.internet import defer
from txjason import handler
from txjason.netstring import JSONRPCServerFactory

class DebugHandler(handler.Handler):
	@handler.exportRPC()
	def ping(self):
		"""
		Check that the connection works. Returns true.
		"""
		return True;


