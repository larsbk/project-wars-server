import time
class logger(object):

	currentId = None
	
	def __init__(self):
		self.parent = None
		self.level = 0

	def setParent(self, p):
		self.parent = p
		self.level = p.level + 1
	
	def getTabs(self):
		t = ""
		for i in range(0, self.level):
			t += "\t"
		return t

	def log(self, m):
		if logger.currentId != str(self):
			logger.currentId = str(self)
			print "**********" + self.getTabs() + str(str(self)) + "********:"

		toPrint = str(m).split("\n")
		f = open("logs/%d.txt"%time.time(), "a")
		for line in toPrint:
			f.write( self.getTabs() + line + "\n")
			print self.getTabs() + "\t" + line
		f.close()



