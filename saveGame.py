from twisted.internet import defer
from bson import ObjectId
from twisted.python import log
from sim.entity import Entity
from sim.map import getMap


@defer.inlineCallbacks
def create(db, game):
	game.gameid = yield db.games.insert({
		"mapid":game.mapid,
		"entityset":game.entityset, 
		"creator":game.creator,
		"entities": [x.getData() for x in
			game.all_entities.values()],
		"turn": 1,
		"history":[],
		"teams":[x.getData() for x in game.teams.values()]
		})

def saveGame(db, game):
	"""
	Saves the entire state of the game to the db.
	"""
	log.msg("save")
	def f(x):
		x["userid"] = ObjectId(x["userid"])
		return x
	teams = [f(x.getData().copy()) for x in game.teams.values() ]
	return db.games.update(
			{"_id" : game.gameid}, {"$set":
				{
					"entities" : [x.getData() for x in game.all_entities.values()],
					"turn" : game.turn,
					"history" : game.history,
					"teams" : teams,
					}
				})

@defer.inlineCallbacks
def loadGame(db, game, gameid):
	"""
	Load a game from the database into the local state
	"""
	log.msg("load")

	#try:
	d = yield db.games.find_one( {"_id":gameid} )
	#except:
	#raise JSONRPCError("no such game")
	m = getMap(d["mapid"]) 
	game.loadMap(m)
	game.loadHistory(d["history"])
	game.loadTurn(d["turn"])
	game.loadCreator(d["creator"])
	game.loadEntityset(d["entityset"])
	game.loadGameid(gameid)
	for e in d["entities"]:
		game.loadEntity(Entity(game, e)) #TODO: loadEntity

	for t in d["teams"]:
		t["userid"] = str(t["userid"])
		game.loadTeam(t)


