from sim.warsexceptions import *
from sim.dataObject import DataObject

class User(DataObject):

	def __init__(self,data, db):
		self.db = db
		super(User, self).__init__(data=data)

	def addCreatedGame(self, game):
		self.db.users.update({"_id":self.data["_id"]}, {
			"$push" : {"games.created":game.getId()}
		})

	def addJoinedGame(self, game):
		self.db.users.update({"_id":self.data["_id"]}, {
			"$push" : {"games.joined":game.getId()}})

	def getId(self):
		return str(self.data["_id"])

	def getObjId(self):
		return self.data["_id"]

	def updatePoints(self, points):
		self.db.users.update({"_id":self.data["_id"]}, {
			"$inc" : {"points":points}})

		

