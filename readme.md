Project wars - Server
=======================

About the game
---------------
Project wars is a online multiplayer turn based strategy game similar to [Advance Wars](http://en.wikipedia.org/wiki/Advance_Wars).
Players take turns moving units atempting to defeat each other. 
If you wish to play the game you will need to use a client. 
There is currently no working client. Have a look at [this repo](https://bitbucket.org/larsbk/warsclientlibpython) if you want to
make a bot for the game.

The protocol
------------
The protocol is [JSONRPC 2.0](http://www.jsonrpc.org/specification). Currently the only supported transport raw tcp where the
messages are separated using [netstrings](http://cr.yp.to/proto/netstrings.txt).
A list of all rpc methods and the documentation of them can be obtained by running `python doc.py addr port`.
A semi-recent version of the output from this utility can be seen [here](https://bitbucket.org/larsbk/project-wars-server/wiki/Protocol).
An example of the communication can be seen [here](https://gist.github.com/LarsBK/2958c6cbb911be61ea32).

Installation
------------
To install this server, you will need python, mongodb, twisted and txjason.

About the server
---------------
Internally the server uses a central database to store the state of all games if progress as well as user info and statistics.
This allows multiple servers to be run for load balancing. This is the reason the server uses a select() based loop rather than
threads; it is easier to just launch multiple servers. The servers can be spread across multiple physical machines without affecting
the user experience as the servers all use the same database. A client can use a single server for all communications or multiple servers
for a single session without adverse effects. A well designed client however, should select a server randomly from a list to assure even
distrubution of clients to the servers. The server might reject new connections if it is overloaded (and the client should try a new server).


