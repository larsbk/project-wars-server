#!/usr/bin/env python
# -*- coding: utf-8 -*-

from twisted.internet import reactor, defer
from twisted.application import service, internet
from sys import argv
import os
import json
#from protocolRaw import RawProtocol, RawProtocolFactory
#from protocolWebSocket import WebSocketProtocol
from autobahn.twisted.websocket import WebSocketServerFactory
from txjason.netstring import JSONRPCServerFactory
from txjason.websocket import JSONRPCServerProtocolWS, JSONRPCServerFactoryWS
from txjason.introspection import IntrospectionHandler
import debugHandler
import authPasswordHandler
import userHandler
import gamesHandler
import gameHandler
import tokenHandler
import txmongo
from txmongo.connection import ConnectionPool
from sim.map import Map
from bson import ObjectId

print "Starting server..."
try:
	f = open("config.json")
	config = json.load(f)
	f.close()
except Exception as e:
	print "Error reading config.json"
	raise e

#reactor.listenTCP(config["port"], RawProtocolFactory())
#wsfactory = WebSocketServerFactory(config["wsaddr"], debug=True)
#wsfactory.protocol = WebSocketProtocol
#listenWS(wsfactory)



print "Loading maps..."
f = open("data/map/list.json")
l = json.load(f)
for k, v in l.items():
	print "\t" + v
	#Load main map data
	f2 = open("data/map/" + v)
	d = json.load(f2)
	n = []
	for line in d["tiles"]:
		n.append( line.replace(" ", ""))
	d["tiles"] = n
	#Load entityset
	for ent in d["entitysets"]:
		if ent not in  Map.entitysetlist:
			print "\t\t" + ent
			f3 = open("data/map/" + ent + "-entityset.json")
			Map.entitysetlist[ent] = json.load(f3)
			f3.close()
	#Load tileset
	if d["tileset"] not in Map.tilesetlist:
		f4 = open("data/tilesets/" + d["tileset"] + "-tileset.json")
		Map.tilesetlist[d["tileset"]] = json.load(f4)
		f4.close()
	m = Map(k, d)
	Map.maplist[k] = m
	f2.close()
f.close()
print "done!"

class WarsJsonEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, ObjectId):
			return str(obj)
		else:
			try:
				return obj.getData()
			except AttributeError:
				return json.JSONEncoder.default(self,obj)

@defer.inlineCallbacks
def start(app):	
	database = yield ConnectionPool(config["dbaddr"]);

	factory = JSONRPCServerFactory(encoder=WarsJsonEncoder)
	factory.addHandler(debugHandler.DebugHandler(),
		namespace="debug");
	factory.addHandler(userHandler.UserHandler(database.wars),
		namespace="user");
	factory.addHandler(authPasswordHandler.PasswordHandler(database.wars),
		namespace="authentication.password");
	factory.addHandler(tokenHandler.TokenHandler(database.wars),
		namespace="tokens");
	factory.addHandler(gamesHandler.GamesHandler(database.wars),
		namespace="games");
	factory.addHandler(gameHandler.GameHandler(database.wars),
		namespace="game");
	factory.addHandler(IntrospectionHandler(factory.service),
		namespace="introspection");

        factoryWS = JSONRPCServerFactoryWS("ws://localhost:" + str(config["websocketPort"]), debug=False)
        factoryWS.setService(factory.service)

	serverNS = internet.TCPServer(config["netstringsPort"], factory);
	serverNS.setServiceParent(app)
	
        serverWS = internet.TCPServer(config["websocketPort"], factoryWS);
	serverWS.setServiceParent(app)
	
	print "Server is listening"

application = service.Application("Project wars server")
start(application)


