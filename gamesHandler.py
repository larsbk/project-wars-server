from twisted.internet import defer
from txjason.service import JSONRPCError
from txjason import handler
from bson import ObjectId
from rpc import requireArgs
from authPasswordHandler import requireLoggedIn
from sim.game import Game
from saveGame import create
from txmongo.filter import sort, DESCENDING

class GamesHandler(handler.Handler):

    games = {}

    def __init__(self, db):
        self.db = db

    @handler.exportRPC(types={"mapid":basestring, "entityset":basestring},
            required=["mapid", "entityset"])
    @requireLoggedIn()
    @defer.inlineCallbacks
    def create(self, user, mapid=None, entityset=None):
        """
        Create a new game with the given settings.
        Returns the id of the new game.
        """
        g = Game()
        g.create(mapid, entityset, user.getId())
        yield create(self.db, g)
        self.games[str(g.getId())] = g
        defer.returnValue(str(g.getId()))
       
    @handler.exportRPC()
    @defer.inlineCallbacks
    def getList(self, limit=10):
        """
        Return a list of games with the given filter applied.
        """
        l = yield self.db.games.find( fields={
            "mapid" : 1,
            "teams" : 1,
            "turn" : 1}, limit=limit,
            filter=sort(DESCENDING("_id")))
        for g in l:
            g["gameid"] = str(g["_id"])
            del g["_id"]
        print(l)
        defer.returnValue(l)


