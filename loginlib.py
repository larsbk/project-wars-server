from Crypto.Cipher import AES
from Crypto.Protocol.KDF import PBKDF2
import Crypto.Random as Random
from time import time

# Zero-knowledge password proof for login!
class Challenge:
	def __init__(self, pw):
		self.pw = pw
		self.timestamp = str(time())[-AES.block_size:].rjust(AES.block_size)
		self.iv = PBKDF2(self.timestamp, HexToByte(self.pw["salt"]), AES.block_size, count=self.pw["iterations"])

		rr = Random.new()
		self._rand_data = rr.read(32)

		cipher = AES.new(HexToByte(self.pw["key"]), AES.MODE_CBC, self.iv)
		self.blob = cipher.encrypt(self._rand_data)

	def GetChallenge(self):
		return { 
				"iterations" : self.pw["iterations"],
				"algo" : self.pw["algo"],
				"blob": ByteToHex(self.blob),
				"timestamp": self.timestamp,
				"salt": self.pw["salt"] }

	def VerifyResponse(self, resp):
		cipher = AES.new(self._rand_data, AES.MODE_CBC, self.iv)
		data = cipher.decrypt(HexToByte(resp["blob"]))
		print "data: " + data
		print "time: " + self.timestamp
		if data == self.timestamp:
			return True
		else:
			return False

def CreateResponse(password, challenge):
	aeskey = PBKDF2(password, HexToByte(challenge["salt"]), 256/8, count=challenge["iterations"])
	iv = PBKDF2(challenge["timestamp"], HexToByte(challenge["salt"]), AES.block_size, count=challenge["iterations"])

	cipher = AES.new(aeskey, AES.MODE_CBC, iv)
	random = cipher.decrypt(HexToByte(challenge["blob"]))
	print "random " + ByteToHex(random)

	cipher = AES.new(random, AES.MODE_CBC, iv)
	
	return { "blob": ByteToHex(cipher.encrypt(challenge["timestamp"])) }

def CreateChkey(password):
	salt =	rr = Random.new().read(32)
	return { 
			"algo" : "PBKDF2",
			"iterations" : 10000,
			"salt": ByteToHex(salt),
			"key": ByteToHex(PBKDF2(password, salt, 256/8, count=10000)) }

def CreateHash(password, salt, count):
	return ByteToHex(PBKDF2(password, HexToByte(salt), 256/8, count=count));

def ByteToHex(bytestr):
	return bytestr.encode("hex")

def HexToByte(hexstr):
	return hexstr.decode("hex")
