from autobahn.websocket import WebSocketServerProtocol
import logger
from transaction import Transaction

class WebSocketProtocol(WebSocketServerProtocol, logger.logger):

	def onOpen(self):
		self.log("connected")
		self.transaction = Transaction(self)

	def __str__(self):
		return "WebSocketProtocol"
		
	def onMessage(self, data, binary):
		"""
		Called by Twisted when new bytes has been received.
		"""
		self.log("got data %s" % data)
		self.transaction.dataReceived(data)

	def kill(self):
		self.sendClose()
	
	def write(self, msg):
		self.sendMessage(msg)
		
	def onClose(self, wasClean, code, reason):
		print "close " + str(self) + ": " + str(reason)
		try:
			self.transaction.connectionLost()
		except AttributeError:
			return


