import os
from twisted.internet import defer
from txjason import handler
from bson import ObjectId
from txjason.service import JSONRPCError
from datetime import datetime,timedelta
from rpc import requireArgs
from user import User
from functools import wraps
from twisted.python import log

def requireLoggedIn(roles=[], maxAge=None):
	def decorator(fn):
		@defer.inlineCallbacks
		@wraps(fn)
		def wrap(self, token=None, **kwargs):
			u = yield TokenHandler.instance.getUserFromToken(token, maxAge);
			for r in roles:
				if not r in u.roles:
					raise PrivilegeError();
			d = yield fn(self, u, **kwargs)
			defer.returnValue( d)
		return wrap
	return decorator

class PrivilegeError(JSONRPCError):
	def __init__(self):
		self.message = "privilege_error";

class TokenExpiredError(JSONRPCError):
	def __init__(self):
		self.message = "token_error";

class TokenHandler(handler.Handler):
	instance = None
	
	def __init__(self, db):
		self.db = db
		TokenHandler.instance = self
		self.cache = {}
		self.users = {}

	def generateToken(self, userid):
		print "generate token"
		token = os.urandom(32).encode("base64")
		self.cache[token] = ObjectId(userid)
		d = {"token":token, "userid":ObjectId(oid=userid)}
		self.db.tokens.insert(d)
		return token
	
	def deleteAllTokens(self, userid):
		self.db.tokens.remove({"userid":ObjectId(oid=userid)})
		self.cache = {} #easier than search for user
	
	@defer.inlineCallbacks
	def getUserFromToken(self, token, maxAge):
		try:
			userid = self.cache[token]
			log.msg("Cached token")
		except KeyError:
			tokenObj = yield self.db.tokens.find({"token":token});
			assert len(tokenObj) <= 1
			if len(tokenObj) == 0:
				raise TokenExpiredError();
			age = datetime.utcnow() - tokenObj[0]["_id"].generation_time.replace(tzinfo=None)
			print "Age: %s" % age
			if maxAge != None and age > maxAge:
				raise TokenExpiredError()
			userid = tokenObj[0]["userid"]
		try:
			u = self.users[userid]
			log.msg("Cached user!", userid)
		except KeyError:
			user = yield self.db.users.find( {"_id":userid} );
			u = User(user[0], self.db)
			self.users[userid] = u
		defer.returnValue(u);

	@handler.exportRPC()
	@requireLoggedIn()
	def getTokens(self, user):
		"""
		Return a list of all your authentication tokens.
		"""
		return self.db.tokens.find( {"username":user["username"]}, fields={"_id":0} );	


