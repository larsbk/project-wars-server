from functools import wraps
from txjason.service import JSONRPCError

def requireArgs(reqargs):
	def decorator(fn):
		@wraps(fn)
		def wrap(*args, **fkwargs):
			for arg in reqargs:
				if not arg in fkwargs:
					raise JSONRPCError("Required argument %s not given" % arg);
			return fn(*args, **fkwargs);
		return wrap
	return decorator;


