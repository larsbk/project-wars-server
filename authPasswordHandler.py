from twisted.internet import defer
from txjason import handler
from txjason.service import JSONRPCError
from bson import ObjectId
from datetime import datetime,timedelta
from rpc import requireArgs
from tokenHandler import TokenHandler, requireLoggedIn

class WrongLoginInfoError(JSONRPCError):
	def __init__(self):
		self.message = "auth_error";

class PasswordHandler(handler.Handler):

	def __init__(self, db):
		self.db = db

	@handler.exportRPC(types={"userid":basestring, "password":basestring}, required=
			["userid", "password"])
	@defer.inlineCallbacks
	def authenticate(self, userid=None, password=None):
		"""
		Authenticate as the user with userid by providing the password.
		Returns a login token that can be used as a proof later.
		"""
		print "auth %s:%s" % (userid, password)
		try:
			i = ObjectId(oid=userid)
		except:
			raise WrongLoginInfoError();
		res = yield self.db.users.find({"_id":i});
		assert not len(res) > 1
		if len(res) == 0:
			raise WrongLoginInfoError();
		if res[0]["authentication"]["password"]["key"] == password:
			print str(res[0]["username"]) + " logged in!"
			defer.returnValue(TokenHandler.instance.generateToken(userid));
		else:
			raise WrongLoginInfoError();
		
	@handler.exportRPC(types={"newPassword":basestring, "userid":basestring}, required=
			["newPassword", "userid"])
	@requireLoggedIn(["admin"])
	def create(self, userid=None, newPassword=None):
		"""
		Create a new password for the given user.
		Requires that you are logged in as an admin.
		"""
		self.db.users.update( {"_id":ObjectId(oid=userid)}, {"$set":{"authentication.password" : {"key" : newPassword}}});
		return True

	@handler.exportRPC(types={"newPassword":basestring}, required=["newPassword"])
	@requireLoggedIn(maxAge=timedelta(0,60))
	def changePassword(self, user, newPassword=None):
		"""
		Change the password. Requires that you are logged in.
		"""
		print "change pw to %s" % newPassword
		self.db.users.update( {"_id":ObjectId(oid=user["_id"])}, {"$set":{"authentication.password.key": newPassword}});
		TokenHandler.instance.deleteAllTokens(user["_id"])
		return True
		
