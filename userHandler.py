from twisted.internet import defer
from txjason.service import JSONRPCError
from txjason import handler
from bson import ObjectId

class NoSuchUserError(JSONRPCError):
	def __init__(self):
		self.message = "invalid_user";

class UserHandler(handler.Handler):
	def __init__(self, db):
		self.db = db;

	@handler.exportRPC(types={"username":basestring})
	@defer.inlineCallbacks
	def getUserid(self, username=None):
		"""
		Return the userid of the user with the given username.
		"""
		r = yield self.db.users.find_one({"username":username}, fields={"_id":1})
		if len(r) == 0:
			raise NoSuchUserError()
		defer.returnValue(str(r["_id"]))
	
	@handler.exportRPC(types={"userid":basestring})
	@defer.inlineCallbacks
	def getUsername(self, userid=None):
		"""
		Return the username of the user with the given userid.
		"""
		r = yield self.db.users.find_one({"_id":ObjectId(oid=userid)}, fields={"username":1});
		if len(r) == 0:
			raise NoSuchUserError()
		defer.returnValue(r["username"])

	@handler.exportRPC(types={"userid":basestring})
	@defer.inlineCallbacks
	def getJoinedGames(self, userid=None):
		r = yield self.db.users.find_one({"_id":ObjectId(oid=userid)},
				fields={"games.joined":1});
		if len(r) == 0:
			raise NoSuchUserError()
		l = []
		for game in r["games"]["joined"]:
			l.append( str(game) )
		defer.returnValue(l)

	@handler.exportRPC(types={"userid":basestring})
	@defer.inlineCallbacks
	def getInvitedGames(self, userid=None):
		r = yield self.db.users.find_one({"_id":ObjectId(oid=userid)},
				fields={"games.invited":1});
		if len(r) == 0:
			raise NoSuchUserError()
		l = []
		for game in r["games"]["invited"]:
			l.append( str(game) )
		defer.returnValue(l)

	@handler.exportRPC()
	@defer.inlineCallbacks
	def getScoreBoard(self):
		r = yield self.db.users.find({}, sort=True, limit=10)
		if len(r) == 0:
			raise NoSuchUserError()
		l = []
		for game in r:
			l.append( {"userid":str(r["userid"]), "points":r["points"]} )
		defer.returnValue(l)


